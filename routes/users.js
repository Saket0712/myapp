var express = require('express');
var router = express.Router();
var Myapi = require('../Models/Myapi')

// for inserting data

router.post('/signup', async (req, res) => {

  try{
      
  const user = await Myapi.findOne( {email: req.body.email} ).exec();
  
  if(user)
  throw new Error('Email is already Registered');


  const newuser = await new Myapi(req.body).save();

  console.log(newuser)
  if(newuser)
  
  
  res.json({message:"New user created successfully", data: newuser, success: true})
  
  else { throw new Error('an err occured during registering') }
  
}

catch (err) {
  console.error(err);
  if (err.message)
  res.json({ message: err.message, data: err, success: false });
  
  else
  res.json({ message: 'error', data: err, success: false })
}

  });

  // for getting data

  router.get('/getAllData', async (req, res) => {
    try{
      const allData = await Myapi.find().exec();

      if(allData){
        res.json({message:"all data", data: allData, success: true})
        console.log(allData)
      }

      else{
        throw new Error("Some error during process")
      }
    }

    catch (err) {
      console.error(err);
      if (err.message)
        res.json({ message: err.message, data: err, success: false });
  
      else
        res.json({ message: 'error', data: err, success: false })
    }
  })


  // for getting one user

  router.get('/getoneData/:id', async (req, res) => {
    try{
      const allData = await Myapi.findById(req.params.id).exec();

      if(allData){
        res.json({message:"all data", data: allData, success: true})
        console.log(allData)
      }

      else{
        throw new Error("Some error during process")
      }
    }

    catch (err) {
      console.error(err);
      if (err.message)
        res.json({ message: err.message, data: err, success: false });
  
      else
        res.json({ message: 'error', data: err, success: false })
    }
  })




module.exports = router;
